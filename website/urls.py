"""website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('beta/', include('lab_1.urls')),
    path('', include('lab_4.urls')),
    path('schedule/', include('lab_5.urls')),
    path('activity/', include('lab_6.urls')),
    path('accordion/', include('lab_7.urls')),
    path('book/', include('lab_8.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
