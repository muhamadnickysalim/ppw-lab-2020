from django.test import Client, TestCase

# Create your tests here.
class Lab_8_test(TestCase):
    def test_page_is_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_8/book.html')