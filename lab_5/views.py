from django.shortcuts import redirect, render
from .forms import *
from .models import *

# Create your views here.
def get_schedule():
    res = []
    temp = Lecture.objects.all()
    for element in temp:
        added = {
            'id' : element.id,
            'name' : element.name,
            'lecturer' : element.lecturer,
            'credits' : element.credits,
            'desc' : element.desc,
            'term' : element.term,
            'room' : element.room,
        }
        res.append(added)
    return res

def schedule(request):
    data = get_schedule()
    return render(request, 'lab_5/schedule.html', {'data' : data})

def del_schedule(request, del_id):
    Lecture.objects.filter(id = del_id).delete()
    return redirect('/schedule/')

def add_schedule(request):
    if request.method == 'GET':
        return render(request, 'lab_5/add_schedule.html', {})
    else:
        form = LectureForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/schedule/')
        else:
            response = {'error' : 'Something went error, please ensure your data is valid and complete!'}
            return render(request, 'lab_5/add_schedule.html', response)