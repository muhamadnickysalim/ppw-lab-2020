# Generated by Django 3.1.1 on 2021-01-07 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_5', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lecture',
            name='room',
            field=models.CharField(max_length=20),
        ),
    ]
