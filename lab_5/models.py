from django.db import models

# Create your models here.
class Lecture(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    lecturer = models.CharField(max_length=100, blank=False, null=False)
    credits = models.PositiveSmallIntegerField(blank=False, null=False)
    desc = models.CharField(max_length=300, blank=False, null= False)
    term = models.CharField(max_length=50, blank=False, null=False)
    room = models.CharField(max_length=20, blank=False, null= False)