from django.urls import path
from .views import *

#url for app
urlpatterns = [
    path('', schedule, name='schedule'),
    path('add', add_schedule, name='add_schedule'),
    path('delete/<int:del_id>', del_schedule, name='del_schedule'),
]