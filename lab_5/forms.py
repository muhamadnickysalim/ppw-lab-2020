from django import forms
from .models import *

class LectureForm(forms.ModelForm):
    class Meta:
        model = Lecture
        fields = ['name', 'lecturer', 'credits', 'desc', 'term', 'room']
