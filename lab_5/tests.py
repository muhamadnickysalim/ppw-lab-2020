from django.test import Client, TestCase
from .forms import *
from .models import *

# Create your tests here.
class Lab_5_test(TestCase):
    def setUp(self):
        self.data = {
            'name' : 'Perancangan Pemrograman Web',
            'lecturer' : 'Arawinda Dinakaramani S.Kom., M.Hum dan Bintang Annisa Bagustari, M.Kom',
            'credits' : 3,
            'desc' : 'Belajar dasar web programing',
            'term' : 'Gasal 2020/2021',
            'room' : 'online',
        }

    def test_form_is_valid(self):
        data = self.data.copy()
        form = LectureForm(data)
        self.assertTrue(form.is_valid())

        data['credits'] = -1
        form = LectureForm(data)
        self.assertFalse(form.is_valid())
    
    def test_schedule_is_exist(self):
        response = Client().get('/schedule/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_5/schedule.html')
    
    def test_add_schedule_is_exist(self):
        response = Client().get('/schedule/add')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_5/add_schedule.html')
    
    def test_schedule_added_and_redirected(self):
        data = self.data.copy()
        response = Client().post('/schedule/add', data)
        self.assertEqual(response.status_code, 302)
        self.assertTemplateNotUsed(response, 'lab_5/add_schedule.html')

        data['credits'] = -1
        response = Client().post('/schedule/add', data)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_5/add_schedule.html')
    
    def test_schedule_generated(self):
        LectureForm(self.data).save()
        response = Client().get('/schedule/')
        self.assertIn('Class', response.content.decode('utf-8'))
    
    def test_schedule_delete(self):
        LectureForm(self.data).save()
        id_test = 1
        response = Client().post('/schedule/delete/%d' %id_test, data = {})
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('lab_5/schedule.html')
        