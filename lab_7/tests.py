from django.test import Client, TestCase

# Create your tests here.
class Test_lab_7(TestCase):
    def test_accordion_is_rendered(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_7/accordion.html')
