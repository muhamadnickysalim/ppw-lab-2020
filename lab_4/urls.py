from django.urls import path
from .views import *

#url for app
urlpatterns = [
    path('', homepage, name='homepage'),
    path('profile', profile, name='profile'),
]