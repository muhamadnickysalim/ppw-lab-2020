from lab_4.views import count_birthday
from django.http import response
from django.test import Client, TestCase

from datetime import date

# Create your tests here.
class Lab_4_unit_test(TestCase):
    def test_index_is_Exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_profile_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)
    
    def test_count_birthday(self):
        date_1 = date(2020,6,1)
        date_2 = date(2020,6,27)
        age_1 = count_birthday(date_1)
        age_2 = count_birthday(date_2)
        self.assertEqual(age_1, 20)
        self.assertEqual(age_2, 21)