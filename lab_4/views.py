from django.shortcuts import render
from datetime import date, datetime
# Create your views here.
def count_birthday(current):
    birth_date = date(1999,6,27)
    age = current.year - birth_date.year
    if (current.month < birth_date.month):
        age -= 1
    elif (current.month == birth_date.month) and (current.day < birth_date.day):
        age -=1
    return age

def homepage(request):
    return render(request, 'lab_4/homepage.html', {})

def profile(request): 
    current = datetime.now()
    age = count_birthday(current)
    response = {
        'age' : age,
    }
    return render(request, 'lab_4/profile.html', response)