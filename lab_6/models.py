from django.db import models

# Create your models here.
class Activity(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    desc = models.CharField(max_length=200, blank=False, null=False)

class Participant(models.Model):
    id_activity = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=50, null=False, blank=False)