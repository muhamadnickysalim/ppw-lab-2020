from django.test import Client, TestCase
from .models import *
from .forms import *

# Create your tests here.
class Lab_6_test(TestCase):
    def setUp(self):
        self.activity_data = {
            'name' : 'Turnamen catur',
            'desc' : 'Turnamen catur antar kampung',
        }
        self.participant_data = {
            'id_activity' : 1,
            'name' : 'Bambang',
        }

    def test_activity_form(self):
        data = self.activity_data.copy()
        self.assertTrue(Form_activity(data).is_valid())
        data['name'] = None
        self.assertFalse(Form_activity(data).is_valid())

    def test_participant_form(self):
        data = self.participant_data.copy()
        self.assertTrue(Form_participant(data).is_valid())
        data['id_activity'] = -1
        self.assertFalse(Form_participant(data).is_valid())

    def test_activity_is_exist(self):
        response = Client().get('/activity/')
        self.assertTemplateUsed(response, 'lab_6/activity.html')
        self.assertEqual(response.status_code, 200)
    
    def test_del_activity(self):
        Form_activity(self.activity_data).save()
        Form_participant(self.participant_data).save()
        self.assertTrue(bool(Activity.objects.all()))
        self.assertTrue(bool(Participant.objects.all()))
        response = Client().post('/activity/del_activity/1', {})
        self.assertEqual(response.status_code, 302)
        self.assertFalse(bool(Activity.objects.all()))
        self.assertFalse(bool(Participant.objects.all()))
    
    def test_add_participant(self):
        Form_activity(self.activity_data).save()
        response = Client().post('/activity/add_participant/1', {'name' : "Bambang"})
        self.assertEqual(response.status_code, 302)
        self.assertTrue(bool(Participant.objects.all()))
    
    def test_del_participant(self):
        Form_activity(self.activity_data).save()
        Form_participant(self.participant_data).save()
        self.assertTrue(bool(Activity.objects.all()))
        self.assertTrue(bool(Participant.objects.all()))
        response = Client().post('/activity/del_participant/1', {})
        self.assertEqual(response.status_code, 302)
        self.assertTrue(bool(Activity.objects.all()))
        self.assertFalse(bool(Participant.objects.all()))