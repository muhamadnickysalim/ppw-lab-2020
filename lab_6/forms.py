from django import forms
from .models import *

class Form_activity(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['name', 'desc']

class Form_participant(forms.ModelForm):
    class Meta:
        model = Participant
        fields = ['id_activity', 'name']