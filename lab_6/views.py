from django.shortcuts import redirect, render
from .forms import *

# Create your views here.
def activity(request):
    response = {}
    return render(request, 'lab_6/activity.html', response)
        

def del_activity(request, del_id):
    Activity.objects.filter(id = del_id).delete()
    Participant.objects.filter(id_activity = del_id).delete()
    return redirect('/activity/')

def add_participant(request, add_id):
    temp = request.POST['name']
    res = {
        'id_activity' : add_id,
        'name' : temp,
    }
    if bool(Activity.objects.filter(id = add_id)):
        Form_participant(res).save()
    return redirect('/activity/')

def del_participant(request, del_code):
    Participant.objects.filter(id = del_code).delete()
    return redirect('/activity/')