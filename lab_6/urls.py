from django.urls import path
from .views import *

#url for app
urlpatterns = [
    path('', activity, name='activity'),
    path('del_activity/<int:del_id>', del_activity, name='del_activity'),
    path('add_participant/<int:add_id>', add_participant, name='add_participant'),
    path('del_participant/<int:del_code>', del_participant, name='del_participant'),
]