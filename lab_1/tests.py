from django.test import TestCase
from django.test import Client

from .views import *
from django.http import HttpRequest
from datetime import date


# Create your tests here.

class Lab1UnitTest(TestCase):

    def test_name_is_changed(self):
        response = Client().get('/beta/')
        html_response = response.content.decode('utf8')
        self.assertIn('<title>' + mhs_name + '</title>', html_response)
        self.assertIn('<h1>Hello my name is ' + mhs_name + '</h1>', html_response)
        self.assertFalse(len(mhs_name) == 0)


    def test_calculate_age_is_correct(self):
        self.assertEqual(0, calculate_age(date.today().year))
        self.assertEqual(21, calculate_age(2000))
        self.assertEqual(31, calculate_age(1990))


    def test_index_contains_age(self):
        response = Client().get('/beta/')
        html_response = response.content.decode('utf8')
        self.assertRegex(html_response, r'<article>I am [0-9]\d+ years old</article>')
