from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhamad Nicky Salim' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 6, 27) #TODO Implement this, format (Year, Month, Date)
npm = 1706039572 # TODO Implement this
# Create your views here.
def beta(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
    return render(request, 'lab_1/beta.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0